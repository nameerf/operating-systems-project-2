/*
* Sto arxeio ayto ylopoioume ton server. H leitourgia tou einai h ekshs:
* Desmeuei ena tmhma (2048 bytes) ths mnhmhs sto opoio apo8hkeuontai oles
* oi plhrofories gia ka8e logarismo. Molis ksekinhsei o server, dexetai klhseis
* apo clients, syndeetai me aytous meso sockets kai gia ka8e aithsh pou lambanei
* apo kapoion client dhmiourgei, meso ths synarthshs systhmatos pthread_create(),
* ena neo nhma to opoio eksyphretei ton ekastote client kai termatizei. Epishs
* stelnetai kai ena "apanthtiko" mhnyma piso ston ekastote client, meso ths synarthshs
* write(), pou eite periexei apla ena enhmerotiko mhnyma eite plhrofories pou zhthse
* o ekastote client. H eksodos apo ton server ginetai me ton syndyasmo ton plhktron
* Ctrl+C. Telos na shmeio8ei oti gia ta sxolia isxuei oti kai ston client
* dhladh gia entoles pou xrhsimopoiountai, idies polles fores, ta sxolia
* gia thn ka8e entolh yparxoun mono sthn proth emfanish ths entolhs.
*/

#include <pthread.h>		/* gia ta threads. */
#include <signal.h>			/* gia ta signals. */

#include "types.h"

#define MEM_SIZE 2048		/* to mege8os pou desmeuoyme gia thn mnhmh. */

/* dhloseis synarthseon. */
void exit_server(int);
void action(void *);
void save_last4transactions(account *, account *, int);
void export_last4transactions(account *);

/* enas char pinakas o opoios xrhsimopoieitai stis synarthseis read() kai write()
 * gia thn anagnosh mhnumaton apo tous clients kai thn eggrafh mhnymaton piso se aytous. */
char buffer[300];

/* orismos tou mutex pou xrhsimopoieitai apo ta threads. */
pthread_mutex_t my_mutex;

/* Synarthsh eksodou apo ton server. Ekteleitai molis "piasei" to shma Ctrl-C. */
void exit_server(int sig) {
	printf("\nExiting server...\n");
	signal(SIGINT, SIG_DFL);

	pthread_mutex_destroy(&my_mutex);   //diagrafh tou mutex.
	pthread_exit(NULL);
	exit(0);
}

/* domh pou periexei plhrofories gia ka8e thread, oi opoies dibibazontai meso
 * ths sunarthshs pthread_create() gia thn xrhsimopoihsh toys sthn synarthsh action(). */
typedef struct thread_data
{
	int socket_desc;	//o descriptor tou socket tou client.
	account *data;		//deikths sthn proth 8esh ths mnhmhs pou einai apo8hkeumenoi oi logariasmoi.
} thread_data;

int main(void)
{
	int sd1, sd2, client_length, len; //metablhtes gia ta sockets.
	struct sockaddr_un local, remote; //domes gia tis die8unseis ton socket tou client kai tou server.

	signal(SIGINT, exit_server); //apeleu8eronei tous "porous" kai termatizei ton server.

	pthread_mutex_init(&my_mutex, NULL);	//arxikopoihsh tou mutex.

	account *acc_data; //deikths typoy account pou deixnei sthn proth 8esh ths mnhmhs sthn opoia apo8hkeuoyme tous logariasmous.

	acc_data = (account *)malloc(MEM_SIZE);	    //desmeush mnhmhs mege8ous MEM_SIZE=2048 bytes gia thn apo8hkeush ton logariasmon.

	/* dhmiourgoume enan kainourgio logariasmo me kena stoixeia kai ton apo8hkeuoume
	 * sthn proth 8esh ths koinhs mnhmhs. ayto to kanoume gia na xrhsimopoioume th metablhth
	 * id os counter gia na metrame posoi logariasmoi exoun apo8hkeutei sthn koinh mnhmh
	 * kai na exoume eukolh prosbash se aytous. */
	account *first_account = (account *)malloc(sizeof(account));
	first_account->id = 0; //arxika exoume 0 apo8hkeumenous logariasmous.
	*acc_data = *first_account; //antigrafei ta periexomena tou first_account sthn proth 8esh ths koinhs mnhmhs.

	if((sd1 = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) { //dhmiourgia tou socket tou server.
		perror("socket");
		exit(1);
	}

	local.sun_family = AF_UNIX;
	strcpy(local.sun_path, SOCK_PATH);
	unlink(local.sun_path); //diagrafh opoion prohgoumenon sockets me to idio onoma pou exoume orisei.
	len = strlen(local.sun_path) + sizeof(local.sun_family);
	/* syndesh tou socket descriptor tou client me ena local port tou L.S. */
	if(bind(sd1, (struct sockaddr *)&local, len) == -1) {
		perror("bind");
		exit(1);
	}

	if(listen(sd1, 5) == -1) { //"akouei" gia kainourgies sundeseis kai tis topo8etei se mia oura mege8ous 5.
		perror("listen");
		exit(1);
	}

	/* atermon broxos. sundeetai me clients pou exoun kanei aithsh gia eksyphrethsh,
	 * diabazei ta dedomena pou exoun steilei meso ths synarthshs read, kai gia ka8e
	 * client, dhmiourgei meso ths pthread_create() ena neo thread to opoio
	 * eksyphretei ton ekastote client kai termatizei. */
	for(;;) {
		int n;
		printf("Waiting for connection...\n");
		client_length = sizeof(remote);
		if((sd2 = accept(sd1, (struct sockaddr *)&remote, &client_length)) == -1) { //pragmatopoihsh syndeshs me client.
			perror("accept");
			exit(1);
		}
		printf("Connected.\n");

		/* apo8hkeuoume tis plhrofories tou thread gia na perastoun os orisma sthn pthread_create. */
		thread_data new_thread_data;
		new_thread_data.socket_desc = sd2;			//to socket descriptor tou client.
		new_thread_data.data = acc_data;			//o deikths sthn proth 8esh ths koinhs mnhmhs.

        /* dhlonoume thn metablhth new_thread sthn opoia apo8hkeuetai to id tou nhmatos pou dhmiourgoume. */
		pthread_t new_thread;

		/* epeidh den 8eloume ta nea nhmata pou dhmiourgountai na perimenoun na teleiosoun
		 * ta prohgoumena orizoume to attribute state os detached. */
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

		/* dhmiourgoume ena neo nhma meso ths pthread_create() me to attribute pou dhmiourghsame parapano gia to nhma.
		 * h synarthsh ekkinhshs einai h synarthsh action() sthn opoia pernietai os orisma h diey8ynsh ths domhs new_thread_data
         * pou periexei to scoket descriptor tou client kai enan deikth sthn proth 8esh ths koinhs mnhmhs. */
		int rc = pthread_create(&new_thread, &attr, (void *)action, (void *)&new_thread_data);
		if(rc) {//elegxoume gia la8h kata thn dhmiourgia ton threads.
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
	}

	return 0;
}

/*
* H synarthsh ekkinhshs tou ekastote nhmatos. Ektelei tis ekshs energeies: apo8hkeush neou logariasmou
* sthn koinh mnhmh, kata8esh xrhmaton se yparxon logariamo sthn koinh mnhmh, analhpsh xrhmaton apo
* yparxon logariasmo sthn koinh mnhmh kai telos enhmerosh tou bibliariou enos yparxontos logariasmou.
* Dexetai os orisma ena deikth typou void ton opoio metatrepoume se thread_data (meso cast) kai pairnoume
* tis metablhtes pou exoun perastei, dhladh to socket descriptor tou client kai ton deikth pou deixnei
* sthn koinh mnhmh. Epishs diabazei meso ths synarthshsh read() to logariasmo pou esteile o client mazi me
* thn energeia pou 8elei na ektelesoume (o server dhladh). An o logariasmos den yparxei sthn koinh mnhmh
* epistrefetai antistoixo mhnyma ston client.
*/
void action(void *thread_arg)
{
	// typonei to id tou thread, monadiko gia ka8e thread.
	printf("Current thread is %lu\n", pthread_self());

	/* pairnoume ta melh ths domhs thread_data sta opoia einai apo8hkeymena, to descriptor tou socket tou client kai
	 * o deikths sthn proth 8esh ths koinhs mnhmhs, kai ta apo8hkeuoyme se topikes metablhtes gia na ta xeiristoume eukolotera. */
	thread_data *th_data = (thread_data *)thread_arg;
	int sd = th_data->socket_desc;
	account *data = th_data->data;

	/* diabazei to "mhnyma", apo8hkeuontas to ston pinaka buffer, me ta dedomena pou esteile o client. */
	int n = read(sd, buffer, 300);
	if(n <= 0) {
		if(n < 0) perror("read");
		exit(1);
	}
	/* an o client 8elei na termatisei kleise to syndedemeno socket, termatise to thread kai epestrepse. */
	if(strcmp((char *)buffer, "exit") == 0) {
		close(sd); //kleisimo tou sindemenou socket apo to thread.
		pthread_exit(NULL); //termatismos tou nhmatos.
		return;
	}

	account *new_account = (account *)buffer;		//o neos logariasmos pou "hr8e" apo ton client.

	switch(new_account->action) {
		case CREATE_ACCOUNT: { //apo8hkeush tou neodhmiourgh8entos logariamsou, pou esteile o client, sthn koinh mnhmh.
			/* elegxoume an exoume xoro sthn koinh mnhmh na apo8hkeusoume ton kainourgio logariasmo
			 * allios den ton apo8hkeuoume kai apo8hkeuoume ston buffer katallhlo mhnyma gia na steiloume
			 * piso ston client enhmeronontas ton. */
			if(sizeof(account) * (data->id+1) > MEM_SIZE) {
				strcpy(buffer, "Account hasn't been created because memory is full!\n");
				break;
			}
			data->id++; //ayksanei ton ari8mo ton logariasmon pou exoun apo8hkeutei sthn koinh mnhmh kata 1.
			int temp = data->id;
			data = data + data->id; //phgainei ston epomeno logariasmo sthn koinh mnhmh.
			new_account->id = temp;
			*data = *new_account; //antigrafei ta periexomena toy logariasmou pou "hr8e" sthn epomenh kenh 8esh ths koinhs mnhmhs.
			strcpy(buffer, "Account created successfully!\n"); //apo8hkeush ston buffer katallhlou mhnymatos gia na stal8ei piso ston client.
		} break;
		case DEPOSIT: { //kata8esh xrhmaton se yparxon logariasmo.
			if(new_account->id == UNKNOWN) { //psaxnoume me to onoma kai to epi8eto
				/* apo8hkeuoyme sthn total_accounts ton synoliko ari8mo ton logariasmon pou einai apo8hkeuemenoi sthn koinh mnhmh. */
				int total_accounts = data->id;
				int i;
				/* "diabazoume" seiriaka tous apo8hkeumenous sthn koinh mnhmh logariasmous kai
				 * elegxoume an kapoios apo aytous einai "idios" (idio onoma kai epi8eto) me ton logarismo
				 * pou esteile o client. */
				for(i=1; i<=total_accounts; i++) {
					data++; //phgainei ston epomeno logariasmo sthn koinh mnhmh.
					/* an to onoma kai to epomymo pou edose o xrhsths yparxoun se logariasmo sthn koinh mnhmh
					 * kane thn kata8esh enhmeronontas ton logariasmo. */
					if((strcmp(new_account->name, data->name) == 0) && (strcmp(new_account->surname, data->surname) == 0)) {
						pthread_mutex_lock(&my_mutex);	//kleidoma tou mutex.
						save_last4transactions(data, new_account, DEPOSIT); //apo8hkeuei thn sygkekrimenh synallagh stis 4 teleutaies.
						data->balance += new_account->amount; //enhmerosnei to upoloipo tou logariasmou.
						data->all_transactions++; //auksanei tis synallages kata 1.
						pthread_mutex_unlock(&my_mutex);	//apeleu8erosh tou mutex.
						strcpy(buffer, "The deposit completed successfully!\n");
						break;
					}
					strcpy(buffer, "Account not found!\n"); //o logariasmos den bre8hke sthn koinh mnhmh. steile analogo mhnyma.
				}
			}
			else { //exoume valid id opote psaxnoume me bash ayto
				if(new_account->id <= data->id) { //an to id tou logariasmou pou psaxnoume uparxei sthn koinh mnhmh kanoume ta idia opos pano.
					pthread_mutex_lock(&my_mutex);	//kleidoma tou mutex.
					save_last4transactions(data+(new_account->id), new_account, DEPOSIT);
					(data+(new_account->id))->balance += new_account->amount;
					(data+(new_account->id))->all_transactions++;
					pthread_mutex_unlock(&my_mutex);	//apeleu8erosh tou mutex.
					strcpy(buffer, "The deposit completed successfully!\n");
				}
				else
					strcpy(buffer, "Account not found!\n");
			}
		} break;
		case WITHDRAW: { //analhpsh xrhmaton apo yparxon logariasmo.
			if(new_account->id == UNKNOWN) { //psaxnoume me to onoma kai to epi8eto.
				int total_accounts = data->id;
				int i;
				for(i=1; i<=total_accounts; i++) {
					data++;
					if((strcmp(new_account->name, data->name) == 0) && (strcmp(new_account->surname, data->surname) == 0)) {
						/* an to poso pou zhth8hke gia anlhpsh einai megalutero tou trexontos ypoloipou
						 * akurose thn synallagh kai apo8hkeuse ston buffer katallhlo mhnyma gia na stal8ei
						 * piso ston client enhmeronontas ton. */
						if(data->balance - new_account->amount < 0)
							strcpy(buffer, "Transaction canceled!\nNot enough money in the account!\n");
						else { //allios sunexise me thn analhpsh.
							pthread_mutex_lock(&my_mutex);	//kleidoma tou mutex.
							save_last4transactions(data, new_account, WITHDRAW);
							data->balance -= new_account->amount;
							data->all_transactions++;
							pthread_mutex_unlock(&my_mutex);	//apeleu8erosh tou mutex.
							strcpy(buffer, "The withdrawal completed successfully!\n");
						}
						break;
					}
					strcpy(buffer, "Account not found!\n");
				}
			}
			else { //exoume valid id opote psaxnoume me bash ayto.
				if(new_account->id <= data->id) {//an to id tou logariasmou pou psaxnoume uparxei sthn koinh mnhmh kanoume ta idia opos pano.
					if((data+(new_account->id))->balance - new_account->amount < 0)
						strcpy(buffer, "Transaction canceled!\nNot enough money in the account!\n");
					else {
						pthread_mutex_lock(&my_mutex);	//kleidoma tou mutex.
						save_last4transactions(data+(new_account->id), new_account, WITHDRAW);
						(data+(new_account->id))->balance -= new_account->amount;
						(data+(new_account->id))->all_transactions++;
						pthread_mutex_unlock(&my_mutex);	//apeleu8erosh tou mutex.
						strcpy(buffer, "The withdrawal completed successfully!\n");
					}
				}
				else
					strcpy(buffer, "Account not found!\n");
			}
		} break;
		case UPDATE_ACCOUNT: { //enhmerosh bibliariou/logariasmou.
			if(new_account->id == UNKNOWN) { //psaxnoume me to onoma kai to epi8eto.
				int total_accounts = data->id;
				int i;
				for(i=1; i<=total_accounts; i++) {
					data++;
					if((strcmp(new_account->name, data->name) == 0) && (strcmp(new_account->surname, data->surname) == 0)) {
						/* afou o logariasmos bre8hke dhmiourghse ena string me tis 4 teleutaies synallages
						 * gia na stal8ei piso ston client. */
						export_last4transactions(data);
						break;
					}
					strcpy(buffer, "Account not found!\n");
				}
			}
			else { //exoume valid id opote psaxnoume me bash ayto.
				if(new_account->id <= data->id)
					export_last4transactions((data+(new_account->id)));
				else
					strcpy(buffer, "Account not found!\n");
			}
		} break;
	}

	/* apanthtiko mhnyma pros ton ekastote client, pou eite periexei apla
	* ena enhmerotiko mhnyma eite plhrofories(update) pou zhthse o client. */
	write(sd, buffer, sizeof(buffer));
	close(sd); //kleisimo tou sindemenou socket apo to thread.

	//termatizoume to trexon nhma.
	pthread_exit(NULL);
}

/*
* Synarthsh h opoia apo8hkeuei thn teleutaia synallagh se mia domh (blepe types.h)
* h opoia krataei tis 4 teleutaies synallages gia ka8e logariasmo. Dexetai os
* orismata dyo deiktes se domh typou account kai enan int me ton typo ths synallaghs
* kata8esh h analhpsh.
*/
void save_last4transactions(account *acc, account *new_acc, int action)
{
	int index = acc->all_transactions;
	/* an oi mexri tora synallages einai ligoteres apo 4 apo8hkeuse tis
	 * synallages pou erxontai stis 8eseis 0,1,2,3. */
	if(index < 4) {
		acc->last4transactions[index].type = action;
		acc->last4transactions[index].balance_before = acc->balance;
		acc->last4transactions[index].amount = new_acc->amount;
	}
	/* allios kane ena shifting stis yparxouses synallages, dhladh apo8hkeuse
	 * thn 2h sthn 1h, thn 3h sthn 2h, thn 4h sthn 3h kai thn teleutaia
	 * synallagh pou hr8e sthn 4h. */
	else {
		acc->last4transactions[0] = acc->last4transactions[1];
		acc->last4transactions[1] = acc->last4transactions[2];
		acc->last4transactions[2] = acc->last4transactions[3];
		/* teleutaia (4h) synallagh. */
		acc->last4transactions[3].type = action;
		acc->last4transactions[3].balance_before = acc->balance;
		acc->last4transactions[3].amount = new_acc->amount;
	}
}

/*
* Synarthsh h opoia "ftiaxnei" meso synenoseon ena megalo string (pinaka me chars) to
* opoio periexei tis plhrofories sxetika me tis 4 teleutaies synallages tou logariasmou
* kai to apo8hkeuei ston pinaka buffer gia na stal8ei piso ston client. H synarthsh
* ayth kaleitai mono otan o client zhthsei enhmerosei bibliariou/logariasmou. Pairnei
* os orisma enan deikth pros th domh account tou pros enhmerosh logariasmou kai den
* epistrefei tipota.
*/
void export_last4transactions(account *acc)
{
	char account_info[200];
	char tmp[20];
	char str[] = "Balance before | Type | Amount | New balance\n";

	strcat(account_info, str);

	int i;
	for(i=0; i<4; i++) {
		if(acc->last4transactions[i].type == UNKNOWN)
			continue;
		sprintf(tmp, "%d |", acc->last4transactions[i].balance_before);
		strcat(account_info, tmp);
		int type = acc->last4transactions[i].type;
		if(type == DEPOSIT) {
			strcat(account_info, " deposit |");
			sprintf(tmp, " %d |", acc->last4transactions[i].amount);
			strcat(account_info, tmp);
			sprintf(tmp, " %d\n", acc->last4transactions[i].balance_before + acc->last4transactions[i].amount);
			strcat(account_info, tmp);
		}
		else if(type == WITHDRAW) {
			strcat(account_info, " withdrawal |");
			sprintf(tmp, " %d |", acc->last4transactions[i].amount);
			strcat(account_info, tmp);
			sprintf(tmp, " %d\n", acc->last4transactions[i].balance_before - acc->last4transactions[i].amount);
			strcat(account_info, tmp);
		}
	}
	strcpy(buffer, account_info);
}
