/*
* Sto arxeio ayto ylopoioume ton client. H leitourgia toy einai h ekshs:
* Emfanizei ena menu me epiloges apo tis opoies o xrhsths (ypallhlos
* trapezas) ektelei tis energeies pou tou exei pei o pelaths ths trapezas.
* Sygkekrimena yparxoun epiloges gia tis ekshs energeies: 1) dhmiourgia neou
* logariasmou, 2) kata8esh xrhmaton, 3) analhpsh xrhmaton kai telos
* 4) enhmerosh logariasmou/bibliarioy. Analoga me thn epilogh pou epilegetai
* o client stelnei mia aithsh eksyphrethshs ston server o opoios apantaei me
* kapoio "enhmerotiko" mhnyma me to an h aithsh eksyphreth8hke epityxos h oxi.
* Na shmeio8ei oti gia entoles pou xrhsimopoiountai, idies polles fores, ta sxolia
* gia thn ka8e entolh yparxoun mono sthn proth emfanish ths entolhs: px.
*
* fgets(input, 100, stdin);//pairnei th eisodo tou xrhsth kai thn apo8hkeuei ston input.
*/

#include "types.h"

/* dhloseis synarthseon */
void show_main_menu(int);
account* create_new_account(void);
account* action(int);

/* global metablhtes */
char input[100];	//enas char pinakas gia tin eisodo toy xristi.
/* enas char pinakas o opoios xrhsimopoieitai sthn synarthsh read
* gia thn anagnosh tou "enhmerotikou" mhnymatos apo ton server. */
char buffer[300];

/*
* Emfanizei to menu me tis epiloges pou exei o xristis. O xrhsths epilegei
* thn energeia pou 8elei na kanei kai afou ginei elegxos gia thn or8othta
* ton dedomenon pou eisagei o xrhsths stelnontai ta dedomena me thn analogh
* energeia pou 8eloume na ektelestei ston server meso ths synarthshs write.
* (Na shmeio8ei oti ylopoihsame diaforetika ton elegxo ton dedomenon apo
* oti zhtousai h ekfonhsh. Sygkekrimena "frontizoume" oste o elegxos na ginetai
* edo (ston client) kai ta dedomena pou stelnontai telika ston server na einai
* "egkura" kai OXI na stelnoume o,ti eisagei o xrhsths ston server, na kanei
* elegxo o server kai na apantaei gia to an ta dedomena einai ok h oxi.)
*/
void show_main_menu(int sd)
{
	printf("\n---------MENU---------\n");
	printf("1. Create new account\n");
	printf("2. Deposit money\n");
	printf("3. Withdraw money\n");
	printf("4. Update account\n");
	printf("5. Exit\n");
	printf("----------------------\n\n");
	printf("Enter your choice below\n");

	get_input:	//label
	printf("client> ");
	fgets(input, 100, stdin); //pairnei th eisodo tou xrhsth kai thn apo8hkeuei ston input.

	/* elegxoume an o xrhsths dosei eisodo me mhkos(tou string) megalutero apo 2
	 * opote kai tou leme na ksanadosei afou h eisodos pou edose den einai egkyrh. */
	if(strlen(input) > 2) {
		printf("Wrong choice.Please enter your choice again\n");
		/* an kai genika den endukneitai h xrhsh ths goto, sthn sygkekrimenh ylopoihsh
		 * pisteuoume pos einai pio praktikh h xrhsh ths goto, ap' to na ylopoiousame
		 * thn sygkekrimenh leitourgia me andromikes klhseis sunarthseon h allios. */
		goto get_input;
	}

	/* analoga me thn eisodo tou xrhsth, kaloume thn antistoixh sunarthsh kai stelnoume
	 * ta dedomena pou eishgage o xrhsths ston server meso ths synarthshs write. */
	switch(input[0]) {
		case '1':
			if(write(sd, create_new_account(), sizeof(account)) == -1) {
				perror("write");
				exit(1);
			}
			break;
		case '2':
			if(write(sd, action(DEPOSIT), sizeof(account)) == -1) {
				perror("write");
				exit(1);
			}
			break;
		case '3':
			if(write(sd, action(WITHDRAW), sizeof(account)) == -1) {
				perror("write");
				exit(1);
			}
			break;
		case '4':
			if(write(sd, action(UPDATE_ACCOUNT), sizeof(account)) == -1) {
				perror("write");
				exit(1);
			}
			break;
		case '5':
			if(write(sd, "exit", sizeof("exit")) == -1) {
				perror("write");
				exit(1);
			}
			break;
		default:
			printf("Wrong choice.Please enter your choice again\n");
			goto get_input;
	}
}

/*
* Synarthsh gia thn dhmiourgia kainourgiou logariasmou. De dexetai kapoio
* orisma kai epistrefei deikth se mia domh typou account (blepe types.h).
* Ginetai enas typikos elegxos gia ta alfari8htika pou eisagei o xrhsths
* kai an ola ta dedomena einai "egkura" epistrefei ena deikth pou deixnei
* ston logariasmo pou molis dhmiourgh8hke.
*/
account* create_new_account(void)
{
	account *new_account; //deikths se domh typou account
	new_account = (account*)malloc(sizeof(account)); //dhmiourgia, sth mnhnh, mias domhs typou account.

	name: //label
	printf("Enter user's first name (max 15 chars): ");
	fgets(input, 100, stdin);
	if(strlen(input) > 2) {//an o xrhsths edose onoma megalutero apo 2 grammata.
		strncpy(new_account->name, input, 15); //antigrafoume thn eisodo tou xrhsth sto melos name ths domhs account.
		new_account->name[15] = '\0'; //pros8etoume ton eidiko xarakthra pou shmainei to telos enos string.
	}
	else {//o xrhsths edose onoma me ena mono gramma opote tou leme na ksanadosei neo onoma.
		printf("You must give a name with al least 2 letters\n");
		goto name;
	}

	surname: //label
	printf("Enter user's last name (max 15 chars): ");
	fgets(input, 100, stdin);
	if(strlen(input) > 2) {//an o xrhsths edose eponumo megalutero apo 2 grammata.
		strncpy(new_account->surname, input, 15); //antigrafoume thn eisodo tou xrhsth sto melos surname ths domhs account.
		new_account->surname[15] = '\0';
	}
	else {//o xrhsths edose eponumo me ena mono gramma opote tou leme na ksanadosei neo eponumo.
		printf("You must give a surname with al least 2 letters\n");
		goto surname;
	}

	balance: //label
	printf("Enter initial account balance: ");
	fgets(input, 100, stdin);
	if(strlen(input) <= 1) {//an o xrhsths pathsei enter xoris na dosei kapoio noumero tou leme na ksanadosei.
		printf("You must give a number with at least one digit!\n");
		goto balance;
	}
	int i;
	/* elegxoume an h eisodos tou xrhsth periexei mono noumera allios ton parapempume na ksanadosei kainourgia eisodo. */
	for(i=0; i<strlen(input)-1; i++) {
		if(isdigit(input[i]) == 0) {//einai pshfio?
			printf("Invalid amount.You must enter only numbers!\n");
			goto balance;
		}
	}
	/* an h eisodos htan egkyrh metatrepoume meso ths atoi to alfari8mitiko
	 * se int kai to apo8hkeuoume sto melos balance ths domhs account. */
	new_account->balance = atoi(input);
	new_account->amount = 0; //otan dhmiourgoyme logariasmo 8eoroume pos den kata8etoume h pairnoume xrhmata
	new_account->all_transactions = 0; //o ari8mos ton synnallagon arxika einai 0
	int j;
	/* arxikopoioume to melos type, ths domhs transaction pou me th seira ths
	 * einai melos ths domhs account, me thn sta8era UNKNOWN. To kanoume ayto
	 * gia na boh8h8oume ston elegxo pou kanoume apo ton server. */
	for(j=0; j<4; j++)
		new_account->last4transactions[j].type = UNKNOWN;

	new_account->action = CREATE_ACCOUNT; //h energeia edo einai dhmiourgia logariasmou

	/* epistrefoume ena deikth typou account pou deixnei ston neodhmiourgh8enta
	 * logariasmo me ta stoixeia pou edose o xrhshs. */
	return new_account;
}

/*
* Synarthsh gia thn energeia pou 8eloume na kanoume apo tis ekshs 3:
* kata8esh, analhpsh, enhmerosh logariamsou. Dexetai san orisma
* mia sta8era typou int apo tis DEPOSIT, WITHDRAW, UPDATE_ACCOUNT
* kai epistrefei ena deikth se domh typou account. H leitourgia
* ths sygkerkimenhs synarthshs einai na parei ta dedomena tou xrhsth
* kai afou ta elenksei dhmiourgei enan neo account(logariasmo) opou
* sto melos toy action exei apo8hkeusei thn epi8umhth energeia. Telos
* gia thn epilogh ths energeias pou 8elei na kanei o xrhsths emfanizetai
* katallhlo menu.
*/
account* action(int act)
{
	char* str1 = "";
	char* str2 = "";
	if(act == DEPOSIT) {
		str1 = "Deposit";
		str2 = "money";
	}
	else if(act == WITHDRAW) {
		str1 = "Withdraw";
		str2 = "money";
	}
	else if(act == UPDATE_ACCOUNT) {
		str1 = "Update";
		str2 = "passbook";
	}

	printf("--------------MENU--------------\n");
	printf("1. %s %s by name+surname\n", str1, str2);
	printf("2. %s %s by account id\n", str1, str2);
	printf("--------------------------------\n\n");
	printf("Enter your choice below\n");

	get_input: //label
	printf("client> ");
	fgets(input, 100, stdin);

	if(strlen(input) > 2) {
		printf("Wrong choice.Please enter your choice again\n");
		goto get_input;
	}

	account *account_to_search; //deikths se domh typou account.
	account_to_search = (account*)malloc(sizeof(account));
	account_to_search->action = act; //apo8hkeusei ths epi8umhths energeias sth metablhth action.

	if((input[0] < '1') || (input[0] > '2')) {
		printf("Wrong choice.Please enter your choice again\n");
		goto get_input;
	}
	else {
		if(input[0] == '1') {
			printf("Enter the name: ");
			fgets(input, 100, stdin);
			strncpy(account_to_search->name, input, 15);
			account_to_search->name[15] = '\0';

			printf("Enter the surname: ");
			fgets(input, 100, stdin);
			strncpy(account_to_search->surname, input, 15);
			account_to_search->surname[15] = '\0';

			/* efoson 8eloume na psaksoume meso onomatos kai eponumou 8etoume to id tou
			 * logariamsou os UNKNOWN(gia eukolia ston elegxo pou kanoume ston server). */
			account_to_search->id = UNKNOWN;
		}
		else if(input[0] == '2') {
			id:
			printf("Enter the account id: ");
			fgets(input, 100, stdin);

			if(strlen(input) <= 1) {
				printf("You must give a number with at least one digit!\n");
				goto id;
			}
			int i;
			for(i=0; i<strlen(input)-1; i++) {
				if(isdigit(input[i]) == 0) {
					printf("Invalid id.You must enter only numbers!\n");
					goto id;
				}
			}
			/* efoson o xrhsths edose egkuro id(dhladh mono ari8mous) to metatrepoume
			 * se int meso ths atoi kai to apo8hkeuoume sto melos id tou account. */
			account_to_search->id = atoi(input);
		}

		/* an h energeia einai kata8esh h analhpsh zhtame apo to xrhsth na dosei kai to poso
		 * pou 8elei na kata8esei h na parei antistoixa. */
		if((act == DEPOSIT) | (act == WITHDRAW)) {
			amount: //label
			printf("How much do you want to %s: ", str1);
			fgets(input, 100, stdin);

			if(strlen(input) <= 1) {
				printf("You must give a number with at least one digit!\n");
				goto amount;
			}
			int i;
			for(i=0; i<strlen(input)-1; i++) {
				if(isdigit(input[i]) == 0) {
					printf("Invalid amount.You must enter only numbers!\n");
					goto amount;
				}
			}
			/* efoson o xrhsths edose egkuro poso(dhladh mono ari8mous) to metatrepoume
			 * se int meso ths atoi kai to apo8hkeuoume sto melos amount tou account. */
			account_to_search->amount = atoi(input);
		}
	}

	return account_to_search;
}

int main(void)
{
	int socket_desc, len, t; //metablhtes gia ta sockets.
	struct sockaddr_un remote; //domh gia th die8unsh tou socket tou server.

	if((socket_desc = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) { //dhmiourgia tou socket tou client.
		perror("socket");
		exit(1);
	}

	printf("Trying to connect...\n");
	remote.sun_family = AF_UNIX;
	strcpy(remote.sun_path, SOCK_PATH);
	len = strlen(remote.sun_path) + sizeof(remote.sun_family);
	/* klhsh systhmatos gia th syndesh tou client kai tou server meso socket. */
	if(connect(socket_desc, (struct sockaddr *)&remote, len) == -1) {
		perror("connect");
		exit(1);
	}
	printf("Connected.\n");

	while(!feof(stdin)) {

		/* kaloume thn synarthshshow_main_menu() me orisma to descriptor tou socket pou dhmiourghsame. */
		show_main_menu(socket_desc);

		/* diabazei kai apo8hkeuei ston pinaka buffer to mhnyma pou stelnei o server.
		 * an to mhkos tou string mhnymatos pou esteile o server einai 8etiko to ektyponoume. */
		if((t = read(socket_desc, buffer, 300)) > 0) {
			buffer[t] = '\0';
			printf("server>\n%s", buffer);
			exit(0);
		}
		else {
			if(t < 0) perror("read"); //o server esteile mhnyma me arnhtiko mhkos == error.
			else printf("Server closed connection\n"); //o server esteile keno mhnyma.
			exit(1);
		}
	}
	close(socket_desc); //kleinoume th sundesh client kai server kleinontas to socket.

	return 0;
}
