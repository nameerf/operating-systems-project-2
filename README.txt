Ομάδα
1) Γιαννακόπουλος Παναγιώτης - 2615
2) Μαρκάκης Εμμανουήλ-Στέφανος - 3970
3) Μπίρμπο Ράντο - 3821

Ο κώδικας γράφτηκε, μεταγλωτίστηκε και αποσφαλματώθηκε σε λειτουργικό σύστημα Arch Linux με τα εξής στοιχεία: kernel-2.6.31.6, gcc-4.4.2, binutils-2.20.
Επίσης τα εκτελέσιμα αρχεία έτρεξαν κανονικά και χωρίς σφάλματα στον διογένη (μέσω ssh) αλλά δεν έγινε μεταγλώτιση και αποσφαλμάτωση του κώδικα στον διογένη.

To apo pano me greeklish
O kodikas grafthke, metaglotisthke kai aposfalmato8hke se leitourgiko systhma Arch Linux me ta ekshs stoixeia: kernel-2.6.31.6, gcc-4.4.2, binutils-2.20.
Epishs ta ektelesima arxeia etreksan kanonika kai xoris sfalmata ston diogenh (meso ssh) alla den egine metaglotish kai aposfalmatosh tou kodika ston diogenh.
